<head>
    <meta charset="utf-8">
    <title>Giovani Mamani</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;600&family=Nunito:wght@600;700;800&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?php echo base_url();?>asset/css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="<?php echo base_url();?>asset/css/style.css" rel="stylesheet">
</head>
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-file-text bg-blue"></i>
                <div class="d-inline">
                    <h5>Postulacion</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    
                    <li class="breadcrumb-item active" aria-current="page">Informacion Usuario</li>
                </ol>
            </nav>
        </div>
    </div>
</div>




                        <div class="row">

                        <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header"><h3 class="text-center">Postulacion </h3><span><center> POSTULACIONES A CARGOS DE TRABAJO</center></span></div>
                                    <div class="card-body text-center">
                                        <div>
                                            <label for="">Cargo N: 1</label>
                                            <div>
                                                Descripcion del cargo:1 Es el cargo de jefe manejo mostrando form llenado<a href="<?php echo base_url();?>Principal/postulacion_jefe"  class="btn btn-primary mr-2">Postular</a>
                                            </div>
                                        </div>
                                        <div>
                                            <label for="">Cargo N: 2</label>
                                            <div>
                                                Descripcion del cargo:2 Es el cargo de sub jefe manejo mostrando lo guardato en bd Modelo<a href="<?php echo base_url();?>Principal/postulacion_subjefe"  class="btn btn-primary mr-2">Postular</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                      
<footer>
                                        <a class="btn btn-icon btn-facebook" href="https://www.facebook.com/"><i class="fab fa-facebook-f"></i></a>
                                        <a class="btn btn-icon btn-twitter" href="https://web.whatsapp.com/"><i class="fab fa-twitter"></i></a>
                                        <a class="btn btn-icon btn-instagram"href="https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin"><i class="fab fa-instagram"></i></a>
</footer>

    <!-- JavaScript Libraries -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    
    <!-- Template Javascript -->
    <script src="<?php echo base_url();?>asset/js/main.js"></script>


                      
